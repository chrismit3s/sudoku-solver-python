.PHONY = all clean out

CC = gcc
CFLAGS = -Wall -pedantic -std=c11

all: out/backtracker.so out/main

clean:
	rm -f out/backtracker.o
	rm -f out/backtracker.so
	rm -f out/main.o
	rm -f out/main
	rmdir out

out:
	mkdir -p out

out/backtracker.o: src/backtracker/backtracker.c out
	$(CC) $(CFLAGS) -fPIC -o $@ -c $<

out/backtracker.so: out/backtracker.o
	$(CC) -W -m64 -shared -o $@ $<

out/main.o: src/backtracker/main.c src/backtracker/backtracker.h out
	$(CC) $(CFLAGS) -o $@ -c $<

out/main: out/main.o out/backtracker.o
	$(CC) $(CFLAGS) -o $@ $^
