#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "backtracker.h"


static bool _backtrack(cell_t[9][9], bool, int, int, solve_seq_t*);
static bool solved(cell_t[9][9], bool);

static bitvec32_t set_cell(cell_t[9][9], bool, index_t, index_t, value_t);
static void unset_cell(cell_t[9][9], bool, index_t, index_t, value_t, bitvec32_t);

static void print_grid(cell_t[9][9], bool);

static int count_possible_values(cell_t);

#define FORMAT_POSSIBLE "%c%c%c%c%c%c%c%c%c"
#define PRINT_POSSIBLE(b) ((((b) >> 1) & 1) ? '1' : ' '),\
                          ((((b) >> 2) & 1) ? '2' : ' '),\
                          ((((b) >> 3) & 1) ? '3' : ' '),\
                          ((((b) >> 4) & 1) ? '4' : ' '),\
                          ((((b) >> 5) & 1) ? '5' : ' '),\
                          ((((b) >> 6) & 1) ? '6' : ' '),\
                          ((((b) >> 7) & 1) ? '7' : ' '),\
                          ((((b) >> 8) & 1) ? '8' : ' '),\
                          ((((b) >> 9) & 1) ? '9' : ' ')

#define FORMAT_BYTE "%c%c%c%c%c%c%c%c"
#define PRINT_BYTE(b) ((((b) >> 0) & 1) ? '1' : '0'),\
                      ((((b) >> 1) & 1) ? '1' : '0'),\
                      ((((b) >> 2) & 1) ? '1' : '0'),\
                      ((((b) >> 3) & 1) ? '1' : '0'),\
                      ((((b) >> 4) & 1) ? '1' : '0'),\
                      ((((b) >> 5) & 1) ? '1' : '0'),\
                      ((((b) >> 6) & 1) ? '1' : '0'),\
                      ((((b) >> 7) & 1) ? '1' : '0')

void free_solve_seq(solve_seq_t *solve_seq) {
	free(solve_seq);
}

solve_seq_t* solve(const char* str, bool colored) {
	//printf("%s\n", str);
	cell_t grid[9][9] = { 0 };
	int missing_cells = 81;

	// setup grid with hints and cells for backtracking
	for (index_t i = 0; i < 81 && str[i] != '\0'; ++i) {
		if (str[i] != '0') {
			set_cell(grid, colored, i / 9, i % 9, str[i] - '0');
			--missing_cells;
		}
	}

	solve_seq_t* solve_seq = (solve_seq_t*)calloc(sizeof(solve_seq_t), missing_cells);

	if (!_backtrack(grid, colored, missing_cells, missing_cells, solve_seq)) {
		free(solve_seq);
		solve_seq = NULL;
	}

	return solve_seq;
}

static bool _backtrack(cell_t grid[9][9], bool colored, int missing_cells, int hist_size, solve_seq_t *solve_seq) {
#ifdef DEBUG
	printf("### %d\n", missing_cells);
	print_grid(grid, true);
	printf("### %d\n", hist_size);
	printf(" => %s\n", solved(grid, colored) ? "true": "false");
#endif /* DEBUG */
	if (missing_cells == 0)
		return solved(grid, colored);

	// find cell with the least possible values
	int min_possible_values = 10, possible_values;
	index_t x = 0, y = 0;
	for (index_t ix = 0; ix != 9; ++ix) {
		for (index_t iy = 0; iy != 9; ++iy) {
			// skip known cells
			if (grid[ix][iy].value_known)
				continue;

			possible_values = count_possible_values(grid[ix][iy]);
			if (min_possible_values > possible_values) {
				min_possible_values = possible_values;
				x = ix;
				y = iy;
			}
		}
	}

	bitvec32_t overwritten;  // temporarily store overwritten bits (for set_cell)
	for (index_t b = 1; b <= 9; ++b) {
		// skip excluded values
		if (GET_BIT(grid[x][y].value, b))
			continue;

		overwritten = set_cell(grid, colored, x, y, b);
		if (_backtrack(grid, colored, missing_cells - 1, hist_size, solve_seq)) {
			*(solve_seq + hist_size - missing_cells) = (solve_seq_t){.x=x, .y=y, .value=b};
			return true;
		}
		unset_cell(grid, colored, x, y, b, overwritten);
	}

	return false;
}

bool solved(cell_t grid[9][9], bool colored) {
	// every cell has to be known
	for (index_t x = 0; x != 9; ++x)
		for (index_t y = 0; y != 9; ++y)
			if (!grid[x][y].value_known)
				return false;

	// check if every group contains every value _once_; as there are only
	// 9 cells in each group and exactly 9 in each group, by the pidgeonhole
	// princible, if any value is duplicated, some value has to be missing
	unsigned short row_groups[9] = { 0 };
	unsigned short column_groups[9] = { 0 };
	unsigned short box_groups[9] = { 0 };
	unsigned short color_groups[9] = { 0 };
	int nx, ny, ix, iy;
	for (index_t n = 0; n != 9; ++n) {  // nth group
		nx = n / 3;
		ny = n % 3;

		for (index_t i = 0; i != 9; ++i) {  // ith cell in nth group
			ix = i / 3;
			iy = i % 3;

			// every cell is known by now (first loop checks for that)
			SET_BIT(row_groups[n],    grid[n][i].value);
			SET_BIT(column_groups[n], grid[i][n].value);
			SET_BIT(box_groups[n],    grid[nx * 3 + ix][ny * 3 + iy].value);
			SET_BIT(color_groups[n],  grid[ix * 3 + nx][iy * 3 + ny].value);
		}
	}
	
	// check that every group has exactly the first 9 bits set
	for (index_t n = 0; n != 9; ++n) {  // nth group
		// check row groups
		if (row_groups[n] != FULL)
			return false;

		// check column groups
		if (column_groups[n] != FULL)
			return false;

		// check box groups
		if (box_groups[n] != FULL)
			return false;

		// check color groups
		if (colored && color_groups[n] != FULL)
			return false;
	}

	return true;
}

bitvec32_t set_cell(cell_t grid[9][9], bool colored, index_t x, index_t y, value_t value) {
	grid[x][y].value_known = 1;
	grid[x][y].value = value;

	index_t box_x = x / 3;
	index_t box_y = y / 3;

	index_t color_x = x % 3;
	index_t color_y = y % 3;

	// backups the overwritten bits for easy backtracking
	bitvec32_t overwritten = 0L;
	// bit indices for overwritten: incremented everytime the value of the current cell
	// in that group is unknown (the cell at x,y will never be processed, as its value
	// is known)
	index_t row_index = 0, column_index = 8, box_index = 16, color_index = 24;

	// two for loops: first one read the values we are going to overwrite, second one
	// overwrites them; if you do it in one loop, some bits be stored in overwritten
	// _after_ they've been overwritten, as the box and the row/colum groups share
	// some cells
	index_t ix, iy, xx, yy;
	for (index_t i = 0; i != 9; ++i) {
		ix = i % 3;
		iy = i / 3;

		// set row cells
		if (!grid[x][i].value_known)
			SET_BIT_TO(overwritten, row_index, GET_BIT(grid[x][i].value, value));
		if (i != y)
			row_index++;

		// set column cells
		if (!grid[i][y].value_known)
			SET_BIT_TO(overwritten, column_index, GET_BIT(grid[i][y].value, value));
		if (i != x)
			column_index++;

		// set box cells
		xx = box_x * 3 + ix;
		yy = box_y * 3 + iy;
		if (!grid[xx][yy].value_known)
			SET_BIT_TO(overwritten, box_index, GET_BIT(grid[xx][yy].value, value));
		if (x != xx || y != yy)
			box_index++;

		// set color cells
		xx = ix * 3 + color_x;
		yy = iy * 3 + color_y;
		if (colored && !grid[xx][yy].value_known)
			SET_BIT_TO(overwritten, color_index, GET_BIT(grid[xx][yy].value, value));
		if (x != xx || y != yy)
			color_index++;
	}
	for (index_t i = 0; i != 9; ++i) {
		ix = i % 3;
		iy = i / 3;

		// set row cells
		if (!grid[x][i].value_known)
			SET_BIT(grid[x][i].value, value);

		// set column cells
		if (!grid[i][y].value_known)
			SET_BIT(grid[i][y].value, value);

		// set box cells
		xx = box_x * 3 + ix;
		yy = box_y * 3 + iy;
		if (!grid[xx][yy].value_known)
			SET_BIT(grid[xx][yy].value, value);

		// set color cells
		xx = ix * 3 + color_x;
		yy = iy * 3 + color_y;
		if (colored && !grid[xx][yy].value_known)
			SET_BIT(grid[xx][yy].value, value);
	}

	return overwritten;
}

void unset_cell(cell_t grid[9][9], bool colored, index_t x, index_t y, value_t value, bitvec32_t overwritten) {
	grid[x][y].value_known = 0;
	grid[x][y].value = 0;

	index_t box_x = x / 3;
	index_t box_y = y / 3;

	index_t color_x = x % 3;
	index_t color_y = y % 3;

	// bit indices for overwritten: incremented everytime the value of the current cell
	// in that group is unknown (the cell at x,y must never be processed, and we have
	// to check for that)
	index_t row_index = 0, column_index = 8, box_index = 16, color_index = 24;
	index_t ix, iy, xx, yy;
	for (index_t i = 0; i != 9; ++i) {
		ix = i % 3;
		iy = i / 3;

		// set row cells
		if (grid[x][i].value_known)
			// recalculate all excluded values for cell at x,y (for all groups)
			SET_BIT(grid[x][y].value, grid[x][i].value);
		else if (i != y)
			// or reset overwritten bits (for all groups)
			SET_BIT_TO(grid[x][i].value, value, GET_BIT(overwritten, row_index));
		if (i != y) row_index++;
		

		// set column cells
		if (grid[i][y].value_known)
			SET_BIT(grid[x][y].value, grid[i][y].value);
		else if (i != x) {
			SET_BIT_TO(grid[i][y].value, value, GET_BIT(overwritten, column_index));
		}
		if (i != x) column_index++;
	

		// set box cells
		xx = box_x * 3 + ix;
		yy = box_y * 3 + iy;
		if (grid[xx][yy].value_known)
			SET_BIT(grid[x][y].value, grid[xx][yy].value);
		else if (xx != x || yy != y) {
			SET_BIT_TO(grid[xx][yy].value, value, GET_BIT(overwritten, box_index));
		}
		if (x != xx || y != yy) box_index++;
		

		// set color cells
		xx = ix * 3 + color_x;
		yy = iy * 3 + color_y;
		if (colored && !grid[xx][yy].value_known)
			SET_BIT(grid[x][y].value, grid[xx][yy].value);
		else if (colored && (xx != x || yy != y))
			SET_BIT_TO(grid[xx][yy].value, value, GET_BIT(overwritten, color_index));
		if (x != xx || y != yy) color_index++;
	}
}

static void print_grid(cell_t grid[9][9], bool detailed) {
	for (index_t x = 0; x != 9; ++x)
		for (index_t y = 0; y != 9; ++y) {
			if (detailed) {
				if (grid[x][y].value_known)
					printf("-%c-", grid[x][y].value + '0');
				else
					printf("%03o", grid[x][y].value >> 1);
				printf("%c", y == 8 ? '\n' : '|');
			} else {
				printf("%c%c", grid[x][y].value_known ? ('0' + grid[x][y].value) : '.',
						y == 8 ? '\n' : ' ');
			}
		}
}

static int count_possible_values(cell_t cell) {
	int num_possible_values = 0;
	for (index_t b = 1; b <= 9; ++b)
		if (!GET_BIT(cell.value, b))
			++num_possible_values;
	return num_possible_values;
}

