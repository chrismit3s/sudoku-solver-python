#include <stdio.h>
#include <stdlib.h>

#include "backtracker.h"


void print_values(value_t grid[9][9]);
bool all_cells_known(value_t grid[9][9]);


int main(void) {
	const char grid_str[] = "030010000106004035005030100001003080003000091002001653008100300560340010314807509";

/*
	value_t grid[9][9] = {  // tdoku easy
		{ 0, 0, 0, 9, 0, 4, 6, 0, 0 },
		{ 0, 4, 0, 0, 0, 0, 8, 3, 1 },
		{ 8, 2, 0, 6, 1, 0, 0, 0, 0 },
		{ 0, 9, 0, 8, 3, 2, 1, 0, 7 },
		{ 2, 1, 8, 7, 4, 5, 0, 0, 0 },
		{ 7, 0, 3, 0, 0, 6, 0, 0, 0 },
		{ 0, 0, 2, 0, 0, 0, 4, 0, 0 },
		{ 1, 8, 5, 4, 2, 9, 0, 6, 0 },
		{ 3, 7, 0, 0, 0, 0, 0, 2, 0 }};
	const char grid_str[] = "000904600040000831820610000090832107218745000703006000002000400185429060370000020";
*/

/*
	value_t grid[9][9] = {  // one cell missing
		{ 6, 5, 2, 4, 8, 3, 9, 1, 7 },
		{ 9, 7, 8, 1, 6, 2, 4, 3, 5 },
		{ 3, 1, 4, 9, 7, 5, 6, 2, 8 },
		{ 8, 2, 5, 7, 3, 6, 1, 4, 9 },
		{ 7, 9, 1, 8, 2, 4, 5, 6, 3 },
		{ 4, 3, 6, 5, 1, 9, 8, 7, 2 },
		{ 2, 6, 9, 3, 4, 8, 7, 5, 0 },
		{ 5, 4, 7, 2, 9, 1, 3, 8, 6 },
		{ 1, 8, 3, 6, 5, 7, 2, 9, 4 }};
	const char grid_str[] = "652483917978162435314975628825736149791824563436519872269348750547291386183657294";
*/

	solve_seq_t *hist = solve(grid_str, true);

	if (hist == NULL) {
		fprintf(stderr, "Backtracking failed\n");
		return EXIT_FAILURE;
	}

	free(hist);
	return EXIT_SUCCESS;
}

void print_values(value_t grid[9][9]) {
	for (index_t x = 0; x != 9; ++x)
		for (index_t y = 0; y != 9; ++y)
			printf("%c%c", grid[x][y] == 0 ? ' ' : ('0' + grid[x][y]),
					y == 8 ? '\n' : ' ');
}

bool all_cells_known(value_t grid[9][9]) {
	for (index_t x = 0; x != 9; ++x)
		for (index_t y = 0; y != 9; ++y)
			if (grid[x][y] == 0)
				return false;
	return true;
}

