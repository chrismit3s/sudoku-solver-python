from os.path import join


LEVEL_BASE = join(".", "levels")
LEVEL_PATH = join(LEVEL_BASE, "{}", "{}.txt")


from src.helpers import rprint  # noqa: F401, E402
from src.backtracker import backtrack  # noqa: F401, E402

from src.game.cell import Cell  # noqa: F401, E402
from src.game.group import Group  # noqa: F401, E402

from src.game.grid import Grid  # noqa: F401, E402

from src.game.gui import GUI  # noqa: F401, E402
