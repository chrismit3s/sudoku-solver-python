from itertools import combinations
from functools import reduce


class Group():
    def __init__(self, grid):
        self.grid = grid
        self.cells = set()
        self.subgroups = set()

    def __str__(self):
        return f"Group({', '.join(sorted(self.missing_values()))})"

    def missing_values(self):
        s = set(cell.value for cell in self.cells if cell.value_known)
        return self.grid.alphabet - s

    def find_subgroups(self):
        # a subgroup is a group of n cells each of which has to contain one of
        # n values; for cells outside this group, those n values can be seen as
        # already used by some cell in this group

        # reset attribute for recalculation
        self.subgroups = set()

        # all cells which dont have a value yet can be in a subgroup
        cells = {cell for cell in self.cells if not cell.value_known}

        # at least 2 cells in a group; at most all but one
        for i in range(2, len(cells)):
            for possible_subgroup in combinations(cells, i):
                subgroup_values = reduce(
                        lambda acc, cell: acc | cell.possible_values(check_subgroups=False),
                        possible_subgroup,
                        set())

                if len(subgroup_values) == i:
                    self.subgroups.add(possible_subgroup)

        return self.subgroups

    def subgroup_exclusions_for(self, cell):
        exclude = set()
        for subgroup in self.subgroups:
            if cell not in subgroup:
                exclude |= reduce(
                        lambda acc, cell: acc | cell.possible_values(check_subgroups=False),  # noqa: E501
                        subgroup,
                        set())
        return exclude

    def add_cell(self, cell):
        self.cells.add(cell)
