from functools import reduce


class Cell():
    def __init__(self, grid, value=None):
        self.grid = grid
        self.value = value
        self.groups = set()

    def __str__(self):
        s = self.possible_values()
        return f"Cell{'!' if self.value_known else '?'}({', '.join(s)})"

    @property
    def value_known(self):
        return self.value is not None

    def possible_values(self, check_subgroups=True):
        if self.value_known:
            return {self.value}

        # intersection of the values missing in each group
        s = reduce(lambda acc, group: acc & group.missing_values(),
                   self.groups,
                   self.grid.alphabet)

        if check_subgroups:
            # check if there are any subgroups whose values we could exclude
            # (see comment in Group.subgroup_exclusions)
            s -= reduce(lambda acc,  group: acc | group.subgroup_exclusions_for(self),
                        self.groups,
                        set())

        return s

    def check(self):
        if self.value_known:
            return False

        # check if theres only one possible value for this cell
        s = self.possible_values()
        if len(s) == 1:
            self.value = s.pop()
            return True
        elif len(s) == 0:
            raise ValueError("Contradiction")

        # check if theres any group in which a value is missing which can only
        # be satisfied by this cell
        for group in self.groups:
            # possible values of remaining cells
            possible_in_rest = reduce(
                    lambda acc, cell: acc | cell.possible_values(),
                    group.cells - {self},
                    set())

            s = self.possible_values()
            s &= group.missing_values() - possible_in_rest
            if len(s) == 1:
                self.value = s.pop()
                return True

        return False

    def add_group(self, group):
        self.groups.add(group)
