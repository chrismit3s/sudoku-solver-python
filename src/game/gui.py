from src import Grid
import colorsys as cs
import cv2
import numpy as np


def write_centered(img, text, mid, color, thickness, font, scale):
    text = str(text)
    x, y = mid
    w, h = cv2.getTextSize(text, font, scale, thickness)[0]
    cv2.putText(img, text, (int(x - w / 2), int(y + h // 2)), font, scale, color, thickness, cv2.LINE_AA)


def fill(img, color, start, end):
    sx, sy = start
    ex, ey = end
    # x and y are swapped because np and cv2 interpret index order differently
    img[sy:ey, sx:ex] = color


class GUI(Grid):
    colors = {"white": (0xFF, 0xFF, 0xFF), "red": (0x00, 0x00, 0xFF), "black": (0x00, 0x00, 0x00)}
    accents = [
        tuple(np.array(cs.hsv_to_rgb(h, 0.3, 1.0))[::-1] * 255) for h in np.linspace(0, 1, 9, endpoint=False)
    ]  # cv2 is bgr
    font = cv2.FONT_HERSHEY_SIMPLEX
    delay = 300

    def __init__(self, *args, size=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.size = size or 720
        self.text_scale = self.size // 360

        # input vars
        self.selection = None
        self.show_hints = True
        self.done = False

        # window setup
        self.filled_frame(GUI.colors["white"])
        self.title = "sudoku-solver"
        cv2.namedWindow(self.title, flags=cv2.WINDOW_GUI_NORMAL)
        cv2.setMouseCallback(self.title, self.mouse_callback)

    def __del__(self, *args, **kwargs):
        cv2.destroyWindow(self.title)

    def filled_frame(self, color):
        self.frame = np.zeros((self.size, self.size, 3), dtype="uint8")
        self.frame[:] = color

    def mouse_callback(self, event, x, y, flags, param):
        if event == cv2.EVENT_LBUTTONUP:
            # x and y are swapped because np and cv2 interpret index order
            # differently
            new_selection = tuple(i * 9 // self.size for i in (y, x))
            if new_selection == self.selection:
                self.selection = None
            else:
                self.selection = tuple(new_selection)

    def key_callback(self, key_code):
        if key_code == -1:
            return
        elif key_code == 27:  # 27 is esc
            self.done = True
            return

        key = chr(key_code).upper()  # only uppercase chars

        if key in self.alphabet and self.selection is not None and key in self.grid[self.selection].possible_values():
            self.grid[self.selection].value = key
            self.selection = None
        elif key == " ":
            self.solve_one()
        elif key == "\r":
            self.solve_all()
        elif key == ".":
            self.show_hints = not self.show_hints

    def draw(self):
        # new frame with white background
        self.filled_frame(GUI.colors["white"])

        # write symbols
        step = self.size // 9
        offset = step // 2
        for i, cell in np.ndenumerate(self.grid):
            # [::-1] because np and cv2 interpret index order differently
            i = np.array(i)[::-1]

            # color cells (if in color mode)
            if self.colored:
                fill(self.frame, GUI.accents[self.color_index(*i)], i * step, (i + 1) * step)

            # write known value
            if cell.value_known:
                write_centered(
                    self.frame,
                    cell.value,
                    i * step + offset,
                    color=GUI.colors["black"],
                    thickness=1,
                    font=GUI.font,
                    scale=self.text_scale,
                )
            # write all possible value for this box if hints are enabled
            elif self.show_hints:
                for v in cell.possible_values():
                    n = sorted(self.alphabet).index(v)
                    j = np.array((n % 3, n // 3), dtype="int")
                    write_centered(
                        self.frame,
                        v,
                        i * step + (j * step + offset) // 3,
                        color=GUI.colors["black"],
                        thickness=1,
                        font=GUI.font,
                        scale=self.text_scale / 3,
                    )

        # black grid
        for i in range(10):
            x = i * self.size // 9
            cv2.line(
                self.frame,
                (0, x),
                (self.size, x),
                color=GUI.colors["black"],
                thickness=4 if (i % 3 == 0) else 1,
                lineType=cv2.LINE_AA,
            )
            cv2.line(
                self.frame,
                (x, 0),
                (x, self.size),
                color=GUI.colors["black"],
                thickness=4 if (i % 3 == 0) else 1,
                lineType=cv2.LINE_AA,
            )

        # draw selection
        if self.selection is not None:
            # [::-1] because np and cv2 interpret index order differently
            i = np.array(self.selection)[::-1]
            j = i + 1

            cv2.rectangle(
                self.frame, tuple(i * step), tuple(j * step), color=GUI.colors["red"], thickness=4, lineType=cv2.LINE_AA
            )

    def show(self):
        cv2.imshow(self.title, self.frame)

    def solve_one(self):
        self.selection = super().solve_one()
        return self.selection

    def solve_all(self):
        for cell in super().solve_iter():
            self.selection = cell
            self.draw()
            self.show()
            self.key_callback(cv2.waitKey(self.delay))

    def loop(self):
        while not self.done:
            self.draw()
            self.show()
            self.key_callback(cv2.waitKey(1))
