from src import Group, Cell, backtrack
import numpy as np


class Grid():
    # box drawing characters
    single = {"|": u"\u2502",
              "-": u"\u2500",
              "+": u"\u253C",  # single hori line crossed with single vert line
              "#": u"\u256B"}  # single hori line crossed with double vert line
    double = {"|": u"\u2551",
              "-": u"\u2550",
              "+": u"\u256A",  # double hori line crossed with single vert line
              "#": u"\u256C"}  # double hori line crossed with double vert line

    # possible alphabets
    numeric = {*"123456789"}
    alphabetic = {*"ABCDEFGHI"}

    def __init__(self, numeric=True, colored=False):
        self.alphabet = Grid.numeric if numeric else Grid.alphabetic
        self.colored = colored
        self.grid = np.array([[Cell(self)
                               for _ in range(9)]
                              for _ in range(9)])
        self.row_groups = [Group(self) for _ in range(9)]
        self.column_groups = [Group(self) for _ in range(9)]
        self.box_groups = [Group(self) for _ in range(9)]
        self.color_groups = ([Group(self) for _ in range(9)]
                             if self.colored
                             else [])

        # for Grid.solve_full
        self.solve_sequence = None

        for (x, y), cell in np.ndenumerate(self.grid):
            self.group_cell(self.row_groups[x], cell)
            self.group_cell(self.column_groups[y], cell)
            self.group_cell(self.box_groups[self.box_index(x, y)],
                            cell)
            if self.colored:
                self.group_cell(self.color_groups[self.color_index(x, y)],
                                cell)

    def box_index(self, x, y):
        return x // 3 + y // 3 * 3

    def color_index(self, x, y):
        return x % 3 + y % 3 * 3

    def group_cell(self, group, cell):
        group.add_cell(cell)
        cell.add_group(group)

    def read_cells_from_stdin(self):
        for row in self.grid:
            # read until a non comment line has been found
            line = "#"
            while line.startswith("#"):
                line = input()

            values = line.strip().split(" ")
            for cell, value in zip(row, values):
                cell.value = value if value in self.alphabet else None

    def read_cells_from_file(self, filename):
        with open(filename, "r") as file:
            lines = file.readlines()
            header = lines[0]

            # check for header
            if not header.startswith("#?"):
                raise ValueError("No setup comment found")

            header = header[2:].strip().lower()
            alphabetic_str, colored_str = header.split()

            # check for valid options
            if alphabetic_str not in {"alphabetic", "numeric"}:
                raise ValueError(f"Unknown option 0: {alphabetic_str}")
            if colored_str not in {"colored", "normal"}:
                raise ValueError(f"Unknown option 1: {colored_str}")

            self.__init__(numeric=(alphabetic_str != "alphabetic"),
                          colored=(colored_str == "colored"))

            # skip comments
            lines = filter(lambda line: not line.startswith("#"), lines)
            for row, line in zip(self.grid, lines):
                values = line.strip().split(" ")
                for cell, value in zip(row, values):
                    cell.value = value if value in self.alphabet else None

    def print_cells(self):
        for x, row in enumerate(self.grid):
            # print number row
            for y, cell in enumerate(row):
                # print number
                print((" " + cell.value) if cell.value_known else "  ",
                      end="")

                # print seperator
                if y != len(self.grid) - 1:
                    lines = Grid.double if y % 3 == 2 else Grid.single
                    print(lines["|"], end="")
                else:
                    print()

            # print seperator row
            if x != len(self.grid) - 1:
                lines = Grid.double if x % 3 == 2 else Grid.single
                for y in range(len(row)):
                    print(lines["-"] * 2, end="")

                    # print seperator
                    if y != len(self.grid) - 1:
                        print(lines["#"
                                    if y % 3 == 2
                                    else "+"],
                              end="")
                else:
                    print()
            else:
                print()

    def find_subgroups(self, groups=None):
        groups = groups or (self.row_groups +
                            self.column_groups +
                            self.box_groups +
                            self.color_groups)
        for group in groups:
            group.find_subgroups()

    def solve_full(self):
        self.solve_sequence = backtrack(self.grid, self.colored, self.alphabet)

    #def _solve_full_rec(self, i):
    #    # check if sudoku is solved
    #    if self.solved():
    #        return []

    #    # full grid, no contradiction, not solved? => weird wtf
    #    if i == 81:
    #        self.print_cells()
    #        raise IndexError("Full grid, no contradiction, not solved?")

    #    # find cell with least amout of possible values
    #    index, cell = min(np.ndenumerate(self.grid),
    #                      key=lambda x: 10 if x[1].value_known else len(x[1].possible_values()))

    #    # immediately go to next cell if this one has a known value
    #    if cell.value_known:
    #        return self._solve_full_rec(i + 1)

    #    try:
    #        # check if the cell can deduce its own value; if it can it has to
    #        # be this value or the mistake is in some cell prior to this one
    #        if cell.check():
    #            ret = self._solve_full_rec(i + 1)
    #            if ret is not None:
    #                ret.append((index, cell.value))
    #            cell.value = None
    #            return ret
    #    except ValueError:  # contradictions
    #        return None

    #    for value in cell.possible_values():
    #        cell.value = value

    #        ret = self._solve_full_rec(i + 1)
    #        if ret is not None:
    #            ret.append((index, value))
    #            cell.value = None
    #            return ret

    #    cell.value = None
    #    return None

    def solve_one(self):
        try:
            # we already know the next cell
            if self.solve_sequence is not None and len(self.solve_sequence) > 0:
                i, value = self.solve_sequence.pop()
                self.grid[i].value = value
                # print("by backtracking previously")
                return i

            # first to to find a cell without expensive subgroup calculations
            for i, cell in np.ndenumerate(self.grid):
                if cell.check():
                    # print("check")
                    return i

            # then try with them
            self.find_subgroups()
            for i, cell in np.ndenumerate(self.grid):
                if cell.check():
                    # print("check with subgroups")
                    return i

            self.solve_full()
            if self.solve_sequence is not None and len(self.solve_sequence) > 0:
                i, value = self.solve_sequence.pop()
                self.grid[i].value = value
                # print("by backtracking")
                return i

            return None
        except ValueError as e:
            raise ValueError(f"Contradiction in cell ({i[0] + 1}|{i[1] + 1}) "
                             f"{cell}") from e

    def solve_iter(self):
        x = True
        while x is not None:
            x = self.solve_one()
            yield x

    def solve_all(self):
        while self.solve_one() is not None:
            pass

    def solved(self):
        for group in (self.row_groups +
                      self.column_groups +
                      self.box_groups +
                      self.color_groups):
            if len(group.missing_values()) != 0:
                return False
        return True
