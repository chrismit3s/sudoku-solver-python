from argparse import ArgumentParser
from multiprocessing import Pool, cpu_count
from src import LEVEL_PATH
from src import rprint
import os
import itertools as it
import requests as rq


URL = "https://sudoku.com/api/getLevel/{}/"


def save_level(filename, *, level_str, difficulty,
               numeric=True,
               colored=False):
    # level_str contains the cells left to right, top to bottom, x for empty
    level_str = level_str.replace("0", "x")
    lines = [level_str[i:i + 9] for i in range(0, 81, 9)]
    lines = [" ".join(list(line)) for line in lines]
    lines.insert(0, (f"#? {'numeric' if numeric else 'alphabetic'} "
                     f"{'colored' if colored else 'normal'}"))

    # write file
    filename = LEVEL_PATH.format(difficulty,
                                 filename)
    os.makedirs(os.path.dirname(filename), exist_ok=True)
    with open(filename, "w+") as file:
        file.writelines(line + "\n" for line in lines)
    return filename


def download_level(difficulty):
    # retry if request fails
    resp = rq.get(URL.format(difficulty))
    while resp.status_code != rq.codes.ok:
        resp = rq.get(URL.format(difficulty))

    desc = resp.json()["desc"]
    level_str = desc[0]
    level_nr = desc[2]
    return save_level(filename=str(level_nr).rjust(3, "0"),
                      level_str=level_str.replace("0", "x"),
                      difficulty=difficulty,
                      numeric=True,
                      colored=False)


# downloads levels from the sudoku.com API
if __name__ == "__main__":
    parser = ArgumentParser()

    parser.add_argument("-d", "--difficulty",
                        nargs="+",
                        choices={"easy", "medium", "hard", "expert"},
                        default=["easy", "medium", "hard", "expert"],
                        help="what difficulties to download",
                        type=str)
    parser.add_argument("-n",
                        required=True,
                        help="how many levels of each difficulty to download",
                        type=int)

    args = parser.parse_args()

    # lazy difficulty iterator
    n = len(args.difficulty) * args.n
    difficulties = it.chain.from_iterable(it.repeat(args.difficulty, args.n))

    # ~3 chunks per cpu core
    cpus = cpu_count()
    chunksize = len(args.difficulty) * args.n // (3 * cpus) + 1

    # run in pool
    with Pool(cpus) as pool:
        rprint("  0%", end="\r", flush=True)
        for i, filename in enumerate(pool.imap_unordered(download_level,
                                                         difficulties,
                                                         chunksize)):
            rprint(f"{100 * i / n:3.0f}% " +
                   f"[{filename.replace(os.sep, '/')}]",
                   end="\r",
                   flush=True)
    rprint("done")
