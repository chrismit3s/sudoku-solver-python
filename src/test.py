from collections import defaultdict
from multiprocessing import Pool, cpu_count
from src import Grid, rprint
from src import LEVEL_BASE
from os.path import join
from time import time
import itertools as it
import os


def extract_difficulty(filename):
    # remove level base folder from filename
    if filename.startswith(LEVEL_BASE):  # (... it should)
        filename = filename[len(LEVEL_BASE) + 1:]  # +1 for path seperator

    try:
        difficulty, filename = filename.split(os.sep)
    except ValueError:
        # filename doesnt follow the directory structure => unknown difficulty
        return filename.split(".")[0]
    return difficulty


def test_level(filename):
    g = Grid()
    g.read_cells_from_file(filename)
    try:
        start = time()
        g.solve_all()
        elapsed = time() - start
    except ValueError:
        return filename, -1
    return filename, elapsed if g.solved() else -1


if __name__ == "__main__":
    fails = defaultdict(lambda: 0)
    times = defaultdict(lambda: 0)
    solves = defaultdict(lambda: 0)
    n = sum(len(files) for *_, files in os.walk(LEVEL_BASE))
    i = 0

    # lazy filename iterator
    filenames = it.chain.from_iterable((join(dirname, file) for file in files)
                                       for dirname, dirs, files
                                       in os.walk(LEVEL_BASE))

    # ~3 chunks per cpu core
    cpus = cpu_count()
    chunksize = n // (3 * cpus) + 1

    # run in pool
    with Pool(cpus) as pool:
        rprint("  0%", end="\r", flush=True)
        for i, (filename, solved_in) in enumerate(pool.imap_unordered(
                test_level, filenames, chunksize)):
            difficulty = extract_difficulty(filename)
            if solved_in < 0:
                rprint(f"failed [{filename.replace(os.sep, '/')}]")
                fails[difficulty] += 1
            else:
                times[difficulty] += solved_in
                solves[difficulty] += 1

            rprint(f"{100 * i / n:3.0f}% " +
                   f"[{filename.replace(os.sep, '/')}]",
                   end="\r",
                   flush=True)
    rprint("done")

    # stats
    total_fails = sum(fails.values())
    if total_fails > 0:
        print("")
        print("fails:")
        print("  " + "\n  ".join(f"{k}: {f}/{f + solves[k]}"
                                 for k, f
                                 in fails.items()
                                 if f > 0))
        print(f"  TOTAL: {total_fails}"
              f"/{sum(fails.values()) + sum(solves.values())}")

    avgs = {k: times[k] / s for k, s in solves.items() if s > 0}
    total_avg = sum(times.values()) / sum(solves.values())
    print("")
    print("times:")
    print("  " + "\n  ".join(f"{k}: {1000 * v:.0f} ms"
                             for k, v
                             in avgs.items()))
    print(f"  AVERAGE: {1000 * total_avg:.0f} ms")
