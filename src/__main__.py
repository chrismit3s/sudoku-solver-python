import argparse as ap
from src import GUI


if __name__ == "__main__":
    parser = ap.ArgumentParser()

    parser.add_argument("--res",
                        dest="size",
                        help="size of the output window",
                        type=int)
    parser.add_argument("-c", "--colored",
                        dest="colored",
                        help="colored sudoku",
                        action="store_true")
    parser.add_argument("-a", "--alphabetic",
                        dest="alphabetic",
                        help="ABCDEFGHI instead of 123456789",
                        action="store_true")

    group = parser.add_mutually_exclusive_group()
    group.add_argument("-f", "--file",
                       dest="filename",
                       default="",
                       help=("read sudoku from file; settings from that file "
                             "override commandline options"),
                       type=str)  # type=ap.FileType("r"))
    group.add_argument("-s", "--stdin",
                       dest="stdin",
                       help="read sudoku from stdin",
                       action="store_true")

    args = parser.parse_args()

    # get options from file
    if args.filename != "":
        with open(args.filename, "r") as file:
            header = file.readline()

            # check for header
            if not header.startswith("#?"):
                raise ValueError("No option comment found")

            header = header[2:].strip().lower()
            alphabetic_str, colored_str = header.split()

            # check for valid options
            if alphabetic_str not in {"alphabetic", "numeric"}:
                raise ValueError(f"Unknown option 0: {alphabetic_str}")
            if colored_str not in {"colored", "normal"}:
                raise ValueError(f"Unknown option 1: {colored_str}")

            args.alphabetic = (alphabetic_str == "alphabetic")
            args.colored = (colored_str == "colored")

    g = GUI(numeric=not args.alphabetic,
            colored=args.colored,
            size=args.size)

    if args.filename != "":
        g.read_cells_from_file(args.filename)
    elif args.stdin:
        g.read_cells_from_stdin()
    else:  # cell values are entered in the gui; start with no hints
        g.show_hints = False

    while True:
        try:
            g.loop()
            break
        except ValueError as e:  # dont close window on contradiction
            print(e)
